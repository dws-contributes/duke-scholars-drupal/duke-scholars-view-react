import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import DukeScholarsProfile from './DukeScholarsProfile';
import * as serviceWorker from './serviceWorker';

const container = document.getElementById('dukeScholarProfile');

ReactDOM.render(
  <DukeScholarsProfile
    uri={container.getAttribute('uri')}
    numofitems={container.getAttribute('numofitems')}
    displaylabel={container.getAttribute('displaylabel')}
    paginate={container.getAttribute('paginate')}
  />, container);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
