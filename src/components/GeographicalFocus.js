import React from 'react';
import ReactDOM from 'react-dom';
import UriContext from '../UriContext';
import FetchDataFromUri from '../Fetcher';

const geographicalFocusRoot = document.getElementById('duke-scholars-view-react-geographical-focus');

class GeographicalFocus extends React.Component {

    static contextType = UriContext;

    constructor(props) {
        super(props);
        this.el = document.createElement('div');
        this.state = {
          data: [],
          error: false
        }
    }

    fetch() {
        let fetcher = new FetchDataFromUri(this.context.uri, 'people', 'geographical_focus', 'all');
        fetcher.fetchData().then(data => this.setState({data: data}))
    }

    componentDidMount() {
      if(geographicalFocusRoot) {
        geographicalFocusRoot.appendChild(this.el);
        this.fetch();
      }
    }

    componentWillUnmount() {
        if(geographicalFocusRoot) {
            geographicalFocusRoot.removeChild(this.el);
        }
    }

    render() {
      if (this.state.data.length) {

        return ReactDOM.createPortal(
          <div>
            {(this.context.displaylabel === 'true') ? <h2>Geographical Focus</h2> : '' }
            <ul>
            {this.state.data.map(function (item, index) {
              return <li key={index}>{item.label} - {item.attributes.focusTypeLabel}</li>
            })}
            </ul>
          </div>,
          this.el
        )
      }
      else return null
    }
}

export default GeographicalFocus;
