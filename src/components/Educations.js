import React from 'react';
import ReactDOM from 'react-dom';
import UriContext from '../UriContext';
import FetchDataFromUri from '../Fetcher';

const educationsRoot = document.getElementById('duke-scholars-view-react-educations');

class Educations extends React.Component {

    static contextType = UriContext;

    constructor(props) {
        super(props);
        this.el = document.createElement('div');
        this.state = {
          data: [],
          error: false
        }
    }

    fetch() {
        let fetcher = new FetchDataFromUri(this.context.uri, 'people', 'educations', 'all');
        fetcher.fetchData().then(data => this.setState({data: data}))
    }

    componentDidMount() {
      if(educationsRoot) {
        educationsRoot.appendChild(this.el);
        this.fetch();
      }
    }

    componentWillUnmount() {
        if(educationsRoot) {
            educationsRoot.removeChild(this.el);
        }
    }

    render() {
      if (this.state.data.length) {

        // we need pagination if the pagination attribute is true and the number of total items in data is bigger than number of items per page
        return ReactDOM.createPortal(
          <div>
            {(this.context.displaylabel === 'true') ? <h2>Education</h2> : '' }
            <ul>
            {this.state.data.reverse().map(function (item, index) {
              return <li key={index}>{item.label} - {item.attributes.institution}</li>
            })}
            </ul>
          </div>,
          this.el
        )
      }
      else return null
    }
}

export default Educations;
