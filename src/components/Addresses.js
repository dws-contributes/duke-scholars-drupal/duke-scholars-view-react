import React from 'react';
import ReactDOM from 'react-dom';
import UriContext from '../UriContext';
import FetchDataFromUri from '../Fetcher';
const addressesRoot = document.getElementById('duke-scholars-view-react-addresses');

class Addresses extends React.Component {

    static contextType = UriContext;

    constructor(props) {
        super(props);
        this.el = document.createElement('div');
        this.state = {
            data: [],
            error: false
        }
    }

    fetch() {
        let fetcher = new FetchDataFromUri(this.context.uri, 'people', 'addresses', 'all');
        fetcher.fetchData().then(data => this.setState({data: data}))
    }

    componentDidMount() {
        if(addressesRoot) {
            addressesRoot.appendChild(this.el);
        }
        this.fetch();
    }

    componentWillUnmount() {
        if(addressesRoot) {
            addressesRoot.removeChild(this.el);
        }
    }

    render() {
      // render if data is not empty
      if (this.state.data.length) {
        return ReactDOM.createPortal(
            <div>
                {(this.context.displaylabel === 'true') ? <h2>Addresses</h2> : '' }
                <ul>
                    {this.state.data.map(function(item, index){
                    return <li key={index}>{item.label}</li>
                    })}
                </ul>
            </div>,
            this.el
        )
      }
      else return null
    }

}

export default Addresses;
