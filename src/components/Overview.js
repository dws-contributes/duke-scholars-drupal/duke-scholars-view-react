import React from 'react';
import ReactDOM from 'react-dom';
import UriContext from '../UriContext';
import FetchDataFromUri from '../Fetcher';
const overviewRoot = document.getElementById('duke-scholars-view-react-overview');

class Overview extends React.Component {

    static contextType = UriContext;

    constructor(props) {
        super(props);
        this.el = document.createElement('div');
        this.state = {
            data: {},
            error: false
        }
    }

    fetchOverview() {
        const uri = this.context.uri;
        let fetcher = new FetchDataFromUri(uri, 'people', 'overview', 5);
        fetcher.fetchData().then(data => this.setState({data: data[0]}));
    }

    componentDidMount() {
        if (overviewRoot) {
            overviewRoot.appendChild(this.el);
        }
        this.fetchOverview();
    }

    componentWillUnmount() {
        if(overviewRoot) {
            overviewRoot.removeChild(this.el);
        }
    }

    render() {
      // render if data is not empty
      if (this.state.data.overview) {
        return ReactDOM.createPortal(
            <div>
              {(this.context.displaylabel === 'true') ? <h2>Overview</h2> : '' }
                <div dangerouslySetInnerHTML={{ __html: this.state.data.overview }}></div>
            </div>,
            this.el
        )
      }
      else return null
    }

}

export default Overview;
