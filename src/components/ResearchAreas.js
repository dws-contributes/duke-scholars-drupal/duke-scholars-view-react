import React from 'react';
import ReactDOM from 'react-dom';
import UriContext from '../UriContext';
import FetchDataFromUri from '../Fetcher';
import Pagination from "rc-pagination";
import '../../node_modules/rc-pagination/assets/index.css';

const researchAreasRoot = document.getElementById('duke-scholars-view-react-research-areas');

class ResearchAreas extends React.Component {

    static contextType = UriContext;

    constructor(props) {
        super(props);
        this.el = document.createElement('div');
        this.state = {
          data: [],
          error: false,
          currentPage: 1,
          offset: 0
        }
    }

    fetch() {
        let fetcher = new FetchDataFromUri(this.context.uri, 'people', 'research_areas', 'all');
        fetcher.fetchData().then(data => this.setState({data: data}))
    }

    onPageChange = page => {
      this.setState({
        currentPage: page,
        offset: (page === (0 || 1)) ? 0 : (((page-1)*(this.context.numofitems)) -1)
      })
    }

    componentDidMount() {
      if(researchAreasRoot) {
        researchAreasRoot.appendChild(this.el);
        this.fetch();
      }
    }

    componentWillUnmount() {
        if(researchAreasRoot) {
            researchAreasRoot.removeChild(this.el);
        }
    }

    render() {
      if (this.state.data.length) {

        // we need pagination if the pagination attribute is true and the number of total items in data is bigger than number of items per page
        let needPagination = !!(this.context.paginate && (this.state.data.length > this.context.numofitems));

        return ReactDOM.createPortal(
          <div>
            {(this.context.displaylabel === 'true') ? <h2>Research Areas</h2> : '' }
            <ul>
            {(needPagination) ?
              (this.state.data.slice(this.state.offset, this.state.offset + Number(this.context.numofitems)).map(function (item, index) {
                return <li key={index}>{item.label}</li>
              })) : (this.state.data.map(function (item, index) {
                return <li key={index}>{item.label}</li>
              }))}
            </ul>
            {(needPagination) ? <Pagination onChange={this.onPageChange} pageSize={this.context.numofitems} total={this.state.data.length} current={this.state.currentPage} /> : '' }
          </div>,
          this.el
        )
      }
      else return null
    }
}

export default ResearchAreas;
