import React from "react";
import ReactDOM from "react-dom";
import UriContext from "../UriContext";
import FetchDataFromUri from "../Fetcher";

const webpagesRoot = document.getElementById(
  "duke-scholars-view-react-webpages"
);

class WebPages extends React.Component {
  static contextType = UriContext;

  constructor(props) {
    super(props);
    this.el = document.createElement("div");
    this.state = {
      data: [],
      error: false,
    };
  }

  fetch() {
    let fetcher = new FetchDataFromUri(this.context.uri, "people", "webpages", "all");
    fetcher.fetchData().then((data) => this.setState({ data: data }));
  }

  componentDidMount() {
    if (webpagesRoot) {
      webpagesRoot.appendChild(this.el);
      this.fetch();
    }
  }

  componentWillUnmount() {
    if (webpagesRoot) {
      webpagesRoot.removeChild(this.el);
    }
  }

  render() {
    if (this.state.data.length) {
      return ReactDOM.createPortal(
        <div>
          {this.context.displaylabel === 'true' ? <h2>Webpages</h2> : ""}
          <ul>
            {this.state.data.map(function (item, index) {
              return (
                <li key={index}>
                  <a href={item.attributes.linkURI} title={item.label}>
                    {item.label}
                  </a>
                </li>
              );
            })}
          </ul>
        </div>,
        this.el
      );
    } else return null;
  }
}

export default WebPages;
