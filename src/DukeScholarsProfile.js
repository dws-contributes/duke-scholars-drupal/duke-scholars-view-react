import React from 'react';
import ErrorBoundary from './components/ErrorBoundary';
import Overview from './components/Overview';
import Publications from './components/Publications';
import UriContext from './UriContext';
import Courses from './components/Courses';
import Newsfeeds from './components/Newsfeeds';
import Grants from './components/Grants';
import ProfessionalActivities from './components/ProfessionalActivities';
import Positions from './components/Positions';
import Addresses from './components/Addresses';
import ResearchAreas from './components/ResearchAreas';
import AcademicPositions from './components/AcademicPositions';
import WebPages from './components/Webpages';
import ArtisticEvents from './components/ArtisticEvents';
import PastAppointments from './components/PastAppointments';
import Educations from './components/Educations';
import GeographicalFocus from './components/GeographicalFocus';
import Gifts from './components/Gifts';
import Licenses from './components/Licenses';

class DukeScholarsProfile extends React.Component {

  render() {
    return (
      <ErrorBoundary>
        <UriContext.Provider value={this.props}>
          <Overview/>
          <Publications/>
          <Courses/>
          <Newsfeeds/>
          <Grants/>
          <ProfessionalActivities/>
          <Positions/>
          <Addresses/>
          <ResearchAreas/>
          <AcademicPositions/>
          <WebPages/>
          <ArtisticEvents/>
          <PastAppointments/>
          <Educations/>
          <GeographicalFocus/>
          <Gifts/>
          <Licenses/>

        </UriContext.Provider>
      </ErrorBoundary>
    );
  }
}

export default DukeScholarsProfile;
