class FetchDataFromUri {

    constructor(uri, type, subType, count) {
        this.uri = uri;
        this.type = (this.types.includes(type)) ? type : 'people';
        this.subType = (this.subtypes.includes(subType)) ? subType : 'overview';
        this.count = (this.countOptions.includes(count)) ? count : 'all';
    }

    data = {};

    endpoint = 'https://scholars.duke.edu/widgets/api/v0.9';

    countOptions = [
        5,
        10,
        15,
        'all'
    ];

    types = [
        'people',
        'organizations'
    ];

    subtypes = [
        'all',
        'publications',
        'artistic_works',
        'awards',
        'courses',
        'newsfeeds',
        'grants',
        'professional_activities',
        'positions',
        'addresses',
        'overview',
        'contact',
        'educations',
        'webpages',
        'gifts',
        'licenses',
        'academic_positions',
        'geographical_focus',
        'past_appointments',
        'artistic_events',
        'research_areas'
    ];

    async fetchData() {
        let url = [this.endpoint, this.type, this.subType, this.count + '.json'].join('/') + '?uri=' + this.uri;
        try {
            const res = await fetch(url);
            return res.json();
        }
        catch (err) {
            console.log('error', err);
        }
    }
}

export default FetchDataFromUri;